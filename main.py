import imaplib
import email
from email.header import decode_header
from telegram import Bot, ParseMode
import logging
import sys
import time
from dotenv import load_dotenv
import os

# Загрузка переменных из файла .env
load_dotenv()

# Параметры для почтового ящика
EMAIL = os.getenv("EMAIL")
PASSWORD = os.getenv("PASSWORD")
MAIL_SERVER = os.getenv("MAIL_SERVER")

# Параметры для бота Telegram
TELEGRAM_BOT_TOKEN = os.getenv("TELEGRAM_BOT_TOKEN")
TELEGRAM_CHAT_ID = os.getenv("TELEGRAM_CHAT_ID")

# Настройка логгера
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Создаем обработчик для вывода в stdout
stdout_handler = logging.StreamHandler(sys.stdout)
stdout_handler.setLevel(logging.INFO)

# Форматтер для вывода в stdout
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
stdout_handler.setFormatter(formatter)

# Добавляем обработчик в логгер
logger.addHandler(stdout_handler)

def check_mail():
    while True:
        try:
            logging.info("Пытаюсь подключиться к почтовому серверу...")
            mail = imaplib.IMAP4_SSL(MAIL_SERVER)
            logging.info("Успешное подключение к почтовому серверу.")

            logging.info("Пытаюсь выполнить вход в почтовый ящик...")
            mail.login(EMAIL, PASSWORD)
            logging.info("Успешный вход в почтовый ящик.")

            mail.select("inbox")

            status, messages = mail.search(None, "UNSEEN")
            if status == "OK":
                for num in messages[0].split():
                    _, msg_data = mail.fetch(num, "(RFC822)")
                    for response_part in msg_data:
                        if isinstance(response_part, tuple):
                            email_message = email.message_from_bytes(response_part[1])
                            subject, encoding = decode_header(email_message["Subject"])[0]
                            if isinstance(subject, bytes):
                                subject = subject.decode(encoding or "utf-8")
                            sender = email.utils.parseaddr(email_message.get("From"))[1]

                            # Логирование получения письма
                            logging.info(f"Получено новое письмо! От: {sender}, Тема: {subject}")

                            # Отправляем уведомление в Telegram
                            bot = Bot(token=TELEGRAM_BOT_TOKEN)
                            message = f"Получено новое письмо!\nОт: {sender}\nТема: {subject}"
                            bot.send_message(chat_id=TELEGRAM_CHAT_ID, text=message, parse_mode=ParseMode.MARKDOWN)

            logging.info("Проверка почты завершена.")
        except Exception as e:
            logging.error(f"Произошла ошибка: {e}")
        finally:
            try:
                mail.close()
                mail.logout()
            except:
                pass

        # Задержка перед следующей проверкой почты (300 секунд = 5 минут)
        time.sleep(300)

if __name__ == "__main__":
    check_mail()