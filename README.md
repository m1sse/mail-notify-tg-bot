# MAIL-NOTIFY-TG-BOT

Бот для отправки уведомлений в телеграм. Каждый 5 минут он проверяет почту, и если есть новые сообщения, уведомление в телеграм
> EXAMPLE
>`Получено новое письмо!
От: shadrin.ar@orionnet.ru
Тема: test12`


## Собрать в docker

> Перед билдом сделать файлик .env со следующим содержимым

```sh
EMAIL=exanple@gmail.ru
PASSWORD=some_password
MAIL_SERVER=imap.gmail.com

TELEGRAM_BOT_TOKEN=YOUR_TOKEN
TELEGRAM_CHAT_ID=CHAT_ID
```
> `Телеграм бота можно получить у @BotFather`
> `Chat-id можно получить написав боту сообщение и отследить изменения в https://api.telegram.org/bot<ТОКЕН_БЕЗ_КАВЫЧЕК_И СКОБОК>/getUpdates`

```sh
sudo docker build \
  --tag=mail-notify-tg-bot:2.1.0 .
```
## Запустить в docker
```sh
sudo docker run \
  --name=mail-notify-tg-bot \
  --detach=true \
  mail-notify-tg-bot:2.1.0
```

## Запустить в docker-compose
```sh
sudo docker compose up -d
```

#### Быстрый запуск без сборки

```sh
sudo docker run \
  --name=mail-notify-tg-bot \
  --detach=true \
  --env-file=.env \
  mail-notify-tg-bot:2.1.0
```